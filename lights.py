from flask import Blueprint, jsonify, request
from qhue import Bridge
import subprocess
import os
import requests
import re

Lights = Blueprint('Lights', __name__)

BRIDGE = {
    'IP': os.environ['HueBridge'],
    'USERNAME': os.environ['HueUser'],
    'b': None
}
WEMO = {
    'IP': os.environ['WemoIp'],
    'PORT': os.environ['WemoPort'] # '49154' '49152' '49153' '49155'
}

def check_connect():
    if BRIDGE['b'] is None:
        BRIDGE['b'] = Bridge(BRIDGE['IP'], BRIDGE['USERNAME'])
    return BRIDGE['b']


@Lights.route('/')
def all_lights():
    b = check_connect()

    lights = b.lights
    lightsData = lights()

    return jsonify(lightsData)


@Lights.route('/<int:lightId>/state/<string:lightAttr>/<attrValue>')
def idv_light_state(lightId, lightAttr, attrValue):
    b = check_connect()
    lights = b.lights
    
    print(lightId)
    print(lightAttr)
    print(attrValue)

    # Default 
    lightData = {'error': 'invalid light attribute'}

    # On/Off State
    if lightAttr == 'on':
        if (attrValue == True or attrValue == 'True' or attrValue == 'true'):
            print('ON')
            _attrValue = True
        else:
            print('OFF')
            _attrValue = False
        lightData = lights[lightId].state(on=_attrValue)


    # Brightnest state
    if lightAttr == 'bri':
        attrValue = int(attrValue)
        _attrValue = attrValue
        if attrValue < 0:
            _attrValue = 0
        elif attrValue > 254:
            _attrValue = 254
        lightData = lights[lightId].state(bri=_attrValue)

    return jsonify(lightData)


@Lights.route('/wemo/')
def wemo_main():
    wemo = WemoControl()
    state = wemo.call('state')
    name = wemo.call('name')
    return jsonify({'name': name, 'state': bool(state)})


@Lights.route('/wemo/<string:call>')
def wemo_on(call):
    wemo = WemoControl()
    res = wemo.call(call)
    return jsonify({'state': res})


class WemoControl:
    def __init__(self):
        self.ip = ip = WEMO['IP']
        self.port = WEMO['PORT']
        self.url = 'http://' + self.ip + ':' + self.port + '/upnp/control/basicevent1'
        self.headers = {
            'Content-Type': 'text/xml',
            'SOAPACTION': '"urn:Belkin:service:basicevent:1#GetBinaryState"'
        }


    def xml_generate(self, call):
        xml_start = '<?xml version="1.0" encoding="utf-8"?><s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><s:Body>'
        xml_end = '</s:Body></s:Envelope>'
        if call == 'state':
            xml_body = '<u:GetBinaryState xmlns:u="urn:Belkin:service:basicevent:1"><BinaryState>1</BinaryState></u:GetBinaryState>'
        elif call == 'on':
            xml_body = '<u:SetBinaryState xmlns:u="urn:Belkin:service:basicevent:1"><BinaryState>1</BinaryState></u:SetBinaryState>'
        elif call == 'off':
            xml_body = '<u:SetBinaryState xmlns:u="urn:Belkin:service:basicevent:1"><BinaryState>0</BinaryState></u:SetBinaryState>'
        else:
            # get friendly name
            xml_body = '<u:GetFriendlyName xmlns:u="urn:Belkin:service:basicevent:1"><FriendlyName></FriendlyName></u:GetFriendlyName>'
        print(call)
        print(xml_start + xml_body + xml_end)
        return xml_start + xml_body + xml_end


    def xml_response(self, call, resp):
        content = resp.decode('utf-8')
        if call == 'name':
            splitStart = '<FriendlyName>'
            splitEnd = '</FriendlyName>'
        else:
            splitStart = '<BinaryState>' 
            splitEnd = '</BinaryState>'

        lsplit = content.split(splitStart, 1)
        rsplit = lsplit[1].split(splitEnd, 1)
        return rsplit[0]


    def http_request(self, call):
        _xml = self.xml_generate(call)
        _headers = self.headers
        if call == "name":
            _headers['SOAPACTION'] = '"urn:Belkin:service:basicevent:1#GetFriendlyName"'
        elif call == 'on' or call == 'off':
            _headers['SOAPACTION'] = '"urn:Belkin:service:basicevent:1#SetBinaryState"'
        return requests.post(self.url, data=_xml, headers=_headers)


    def call(self, call):
        req = self.http_request(call)
        resData = self.xml_response(call, req.content)

        return resData

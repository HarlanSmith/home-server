from flask import Flask, render_template
from lights import Lights
from botvac import Botvac
from router import Router
from weather import Weather

app = Flask(__name__, static_url_path='')
app.register_blueprint(Lights, url_prefix="/lights")
app.register_blueprint(Botvac, url_prefix="/botvac")
app.register_blueprint(Router, url_prefix="/router")
app.register_blueprint(Weather, url_prefix="/weather")

@app.route('/')
def Main_App():
    return app.send_static_file('webapp/index.html')

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')

from flask import Blueprint, jsonify, request
import requests
import os
Weather = Blueprint('Weather', __name__)

WEATHER = {
    'key': os.environ['WeatherKey'],
    'baseUrl': 'https://api.forecast.io/forecast/',
    'location': {
        'latitude': '30.352795',
        'longitude': '-97.759262'
    },
}



@Weather.route('/')
def all_weather():
    url = WEATHER['baseUrl'] + WEATHER['key'] + '/' + WEATHER['location']['latitude'] + ',' + WEATHER['location']['longitude']
    payload = {'extend': 'hourly'}
    r = requests.get(url, params=payload)
    weatherData = r.json()

    return jsonify(weatherData)




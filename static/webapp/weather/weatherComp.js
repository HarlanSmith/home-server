/********************
| WEATHER COMPONENT |
********************/

app.factory('weatherHttpFactory', ['$http', function($http) {
    var retObj = {};

    retObj['getWeather'] = function() {
        var url = "/weather/";
        return $http.get(url).then(function(response) {
            // console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    return retObj;

}]);

app.factory('weatherDataFactory', [function() {
    var retObj = {};

    var _graphShell = {
        'labels': [],
        'series': [],
        'data': [],
        'options': {
            responsive: true,
            maintainAspectRatio: false,
            'scales': {
                'yAxes': [{
                    'display': false
                }],
                'xAxes': [{
                    'display': false,
                    'gridLines': {
                        'display': false
                    },
                    'afterTickToLabelConversion': function(data){
                        var xLabels = data.ticks;

                        xLabels.forEach(function (labels, i) {
                            if (i % 4 != 0){
                                xLabels[i] = '';
                            }
                        });
                    }

                }]
            }

        },
        'colours': [],
        'dataSets': {
            'labels': [],
            'temperature': {
                'data': [],
                'series': [],
                'colours': [
                    '#FFA000'
                ]
            },
            'precipitation': {
                'data': [],
                'series': [],
                'colours': [
                    '#1976D2'//#2196F3'
                ]
            },
            'wind': {
                'data': [],
                'series': [],
                'colours': [
                    '#607D8B'
                ]
            }
        }
    };

    retObj['mapIcon'] = function(sourceIcon) {
        var retIcon = 'wi-day-sunny'; // Default
        var iconDict = {
            'clear-day': 'wi-day-sunny', 
            'clear-night': 'wi-night-clear', 
            'rain': 'wi-rain', 
            'snow': 'wi-snow', 
            'sleet': 'wi-sleet', 
            'wind': 'wi-strong-wind', 
            'fog': 'wi-fog', 
            'cloudy': 'wi-cloudy', 
            'partly-cloudy-day': 'wi-day-cloudy',
            'partly-cloudy-night': 'wi-night-alt-cloudy',
            'hail': 'wi-hail',
            'thunderstorm': 'wi-thunderstorm',
            'tornado': 'wi-tornado'
        };
        try {
            retIcon = iconDict[sourceIcon];
        } catch(err) {
            console.log(err);
        }

        return retIcon;
    };

    retObj['getIconBackground'] = function() {
        var _currTime = new Date().getHours();
        return (_currTime >= 19 || _currTime <= 6) ? 'night' : 'day';
    };

    function formatHour(date) {
      var hours = date.getHours();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      return hours + ' ' + ampm;
    }


    function findNextInArray(array, item, current) {
        return array.indexOf(item, current + 1);
    }

    retObj['getHourlyGraph'] = function(hourlyObj) {
        var retData = angular.copy(_graphShell);
        
        retData.dataSets.temperature.series = ["Temperature"];
        retData.dataSets.precipitation.series = ["Precipitation"];
        retData.dataSets.wind.series = ["Wind"];
        angular.forEach(hourlyObj.data, function(value, key) {
            // Calc Label
            retData.dataSets.labels[key] = formatHour(new Date(value.time * 1000));
            // Map Temp
            retData.dataSets.temperature.data[key] = value.temperature;
            // Map Precip
            retData.dataSets.precipitation.data[key] = value.precipProbability;
            // Map Wind
            retData.dataSets.wind.data[key] = value.windSpeed;
        });

        return retData;
    };

    retObj['switchActiveGraph'] = function(graphData, switchType) {
        var retData = {
            'series': graphData.dataSets[switchType].series, //.slice(0, 96),
            'labels': graphData.dataSets.labels,//.slice(0, 96),
            'data': [[graphData.dataSets[switchType].data][0]],//.slice(0, 96)],
            'options': graphData.options,
            'colours': graphData.dataSets[switchType].colours
        };

        return retData;
    };

    retObj['setActiveDay'] = function(graphData, weatherData, dayIndex) {
        var retData = {
            'marginLeft': '0',
            'dayIndex': dayIndex,
            'dayData': weatherData.currently 
        };
        //console.log(graphData);
        if (dayIndex != 0) {
            // Calc Px Width Per Graph Hour
            var singlePx = 398 / 24; // TODO make dynamic based off widget width
            var currentIndex = 0;
            for(var i = 0; i<dayIndex; i++) {
                currentIndex = findNextInArray(graphData, '12 am', currentIndex);
            }
            //console.log(currentIndex);
            retData.marginLeft = '-' + Math.round(currentIndex * singlePx).toString() + 'px';
            retData.dayData = weatherData.daily.data[dayIndex];
            retData.dayData.temperature = retData.dayData.temperatureMax;
            retData.dayData['iconMap'] = retObj.mapIcon(retData.dayData.icon);
            retData.dayData['iconBackground'] = retObj.getIconBackground();
        }

        return retData;
    };

    return retObj;

}]);

app.controller('weatherCtrl', ['$scope', 'weatherHttpFactory', 'weatherDataFactory', function($scope, weatherHttpFactory, weatherDataFactory) {
    var wt = this;
    wt.title = 'Weather';
    wt.weatherDataFactory = weatherDataFactory;
    wt.fetchingData = false;
    wt.weatherData = {};
    wt.activeGraph = {};
    wt.activeDay = {};
    wt.graphFilter = {};
    wt.graphFilter['active'] = 'temperature';
    wt.graphFilter['set'] = function(type) {
        wt.activeGraph = weatherDataFactory.switchActiveGraph(wt.weatherData.graph, type);
        wt.graphFilter['active'] = type;
    };

    $scope.$watch('wt.graphFilter.active', function(newValue, oldValue) {
        if (newValue != oldValue) {
            wt.graphFilter.set(newValue);
        }
    });
    
    wt.setActiveDay = function(dayIndex) {
        wt.activeDay = weatherDataFactory.setActiveDay(wt.activeGraph.labels, wt.weatherData, dayIndex);
        console.log(wt.activeDay);
    };

    wt.init = function() {
        wt.fetchingData = true;
        // Make HTTP Request to Fetch Weather Data
        var _prom1 = weatherHttpFactory.getWeather();

        _prom1.then(function(response) {
            //console.log(response.data);
            var _data = response.data;

            // Set Custom Variables
            _data.currently['iconMap'] = weatherDataFactory.mapIcon(_data.currently.icon);
            _data.currently['iconBackground'] = weatherDataFactory.getIconBackground();
            _data['graph'] = weatherDataFactory.getHourlyGraph(_data.hourly);
            
            // wt.activeGraph = weatherDataFactory.switchActiveGraph(_data.graph, "temperature");
            console.log(_data);
            console.log(wt.activeGraph);
            wt.weatherData = _data;
            wt.graphFilter.set("temperature");
            wt.setActiveDay(0);
            wt.fetchingData = false;

        });
    };

    wt.init();
}]);

app.component('weatherComp', {
    controller: 'weatherCtrl',
    controllerAs: 'wt',
    templateUrl: 'webapp/weather/weather.tpl.html'
});


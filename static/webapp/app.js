'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('HomeAutomator', [
    'ui.router',
    'ui.bootstrap',
    'ngMaterial',
    'chart.js'
]);


app.config(['$stateProvider', '$urlRouterProvider', 'ChartJsProvider', function($stateProvider, $urlRouterProvider, ChartJsProvider) {
    //$locationProvider.hashPrefix('!');

    // For any unmatched url, redirect to /
    //$urlRouterProvider.otherwise("/");

    $stateProvider
    .state('Main', {
      url: "/"
      //templateUrl: "partials/state1.html"
    });

    ChartJsProvider.setOptions({
        // Boolean - Whether to animate the chart
        //animation: true,
        // Number - Number of animation steps
        //animationSteps: 60,
        // String - Animation easing effect
        //animationEasing: "easeOutQuart",
        // Boolean - If we should show the scale at all
        showScale: true,
        // Boolean - If we want to override with a hard coded scale
        scaleOverride: false,
        // ** Required if scaleOverride is true **
        // Number - The number of steps in a hard coded scale
        scaleSteps: null,
        // Number - The value jump in the hard coded scale
        scaleStepWidth: null,
        // Number - The scale starting value
        scaleStartValue: null,
        // String - Colour of the scale line
        scaleLineColor: "rgba(0,0,0,.1)",
        // Number - Pixel width of the scale line
        scaleLineWidth: 1,
        // Boolean - Whether to show labels on the scale
        scaleShowLabels: true,
        // Interpolated JS string - can access value
        scaleLabel: "<%=value%>",
        // Boolean - Whether the scale should stick to integers, and not show any floats even if drawing space is there
        scaleIntegersOnly: true,
        // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: false,
        // String - Scale label font declaration for the scale label
        scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        // Number - Scale label font size in pixels
        scaleFontSize: 12,
        // String - Scale label font weight style
        scaleFontStyle: "normal",
        // String - Scale label font colour
        scaleFontColor: "#666",
        // Boolean - whether or not the chart should be responsive and resize when the browser does.
        responsive: false,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        // Boolean - Determines whether to draw tooltips on the canvas or not - attaches events to touchmove & mousemove
        showTooltips: true,
        // Boolean - Determines whether to draw built-in tooltip or call custom tooltip function
        customTooltips: false,
        // Array - Array of string names to attach tooltip events
        tooltipEvents: ["mousemove", "touchstart", "touchmove", "mouseout"],
        // String - Tooltip background colour
        tooltipFillColor: "rgba(0,0,0,0.8)",
        // String - Tooltip label font declaration for the scale label
        tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        // Number - Tooltip label font size in pixels
        tooltipFontSize: 14,
        // String - Tooltip font weight style
        tooltipFontStyle: "normal",
        // String - Tooltip label font colour
        tooltipFontColor: "#fff",
        // String - Tooltip title font declaration for the scale label
        tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
        // Number - Tooltip title font size in pixels
        tooltipTitleFontSize: 14,
        // String - Tooltip title font weight style
        tooltipTitleFontStyle: "bold",
        // String - Tooltip title font colour
        tooltipTitleFontColor: "#fff",
        // String - Tooltip title template
        tooltipTitleTemplate: "<%= label%>",
        // Number - pixel width of padding around tooltip text
        tooltipYPadding: 6,
        // Number - pixel width of padding around tooltip text
        tooltipXPadding: 6,
        // Number - Size of the caret on the tooltip
        tooltipCaretSize: 8,
        // Number - Pixel radius of the tooltip border
        tooltipCornerRadius: 6,
        // Number - Pixel offset from point x to tooltip edge
        tooltipXOffset: 10,
        // String - Template string for single tooltips
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
        // String - Template string for single tooltips
        multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>",
        // String - Colour behind the legend colour block
        multiTooltipKeyBackground: '#fff',
        // Array - A list of colors to use as the defaults
        segmentColorDefault: ["#A6CEE3", "#1F78B4", "#B2DF8A", "#33A02C", "#FB9A99", "#E31A1C", "#FDBF6F", "#FF7F00", "#CAB2D6", "#6A3D9A", "#B4B482", "#B15928" ],
        // Array - A list of highlight colors to use as the defaults
        segmentHighlightColorDefaults: [ "#CEF6FF", "#47A0DC", "#DAFFB2", "#5BC854", "#FFC2C1", "#FF4244", "#FFE797", "#FFA728", "#F2DAFE", "#9265C2", "#DCDCAA", "#D98150" ],
        // Function - Will fire on animation progression.
        onAnimationProgress: function(){},
        // Function - Will fire on animation completion.
        onAnimationComplete: function(){
            // console.log('YUP');
            // var ctx = this.chart.ctx;
            // ctx.font = this.scale.font;
            // ctx.fillStyle = this.scale.textColor
            // ctx.textAlign = "center";
            // ctx.textBaseline = "bottom";

            // this.datasets.forEach(function (dataset) {
            //     dataset.points.forEach(function (points) {
            //         ctx.fillText(points.value, points.x, points.y - 10);
            //     });
            // });
        }
      });
}]);


app.controller('MainCtrl', function () {
    this.title = 'Main App';
});


// This filter makes the assumption that the input will be in decimal form (i.e. 17% is 0.17).
app.filter('percentage', ['$filter', function ($filter) {
  return function (input, decimals) {
    return $filter('number')(input * 100, decimals) + '%';
  };
}]);
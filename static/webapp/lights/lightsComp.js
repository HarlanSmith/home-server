/*******************
| LIGHTS COMPONENT |
*******************/

app.factory('lightsHttpFactory', ['$http', function($http) {
    var retObj = {};

    retObj['getLights'] = function() {
        var url = "/lights/";
        return $http.get(url).then(function(response) {
            console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    retObj['setLightState'] = function(lightId, state, value) {
        var url = "/lights/" + lightId + "/state/" + state + "/" + value;
        $http.get(url).then(function(response) {
            console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    retObj['getWemoLight'] = function() {
        var url = "/lights/wemo/";
        return $http.get(url).then(function(response) {
            console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    retObj['toggleWemoLight'] = function(state) {
        call = (state) ? 'on' : 'off';
        var url = "/lights/wemo/" + call;
        return $http.get(url).then(function(response) {
            console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    return retObj;

}]);

app.controller('lightsCtrl', ['$http', 'lightsHttpFactory', function($http, lightsHttpFactory) {
    var lc = this;
    lc.title = 'Lights';
    lc.lightsData = false; // Philips Hue
    lc.wemoLightData = false; // WeMo Switch

    window.testScope = function() {
        console.log(lc);
    };

    lc.init = function() {
        // Make HTTP Request to Fetch Lights Data
        var _prom1 = lightsHttpFactory.getLights();
        var _prom2 = lightsHttpFactory.getWemoLight();

        _prom1.then(function(response) {
            console.log(response.data);
            lc.lightsData = response.data;
        });
        _prom2.then(function(response) {
            console.log(response.data);
            lc.wemoLightData = response.data;
        }); 
    }

    // Hue State Change
    lc.changeLightStatus = function(lightId, state) {
        if (state == 'on') {
            var value = lc.lightsData[lightId].state.on;
        } else if (state == 'bri') {
            var value = lc.lightsData[lightId].state.bri;
            console.log(value);
        };
        lightsHttpFactory.setLightState(lightId, state, value);
    };


    // WeMo State Change
    lc.toggleWemoLight = lightsHttpFactory.toggleWemoLight;

    lc.init();
}]);

app.component('lightsComp', {
    controller: 'lightsCtrl',
    controllerAs: 'lc',
    templateUrl: 'webapp/lights/lights.tpl.html'
});


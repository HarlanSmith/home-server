/*******************
| BOTVAC COMPONENT |
*******************/

app.factory('botvacHttpFactory', ['$http', function($http) {
    var retObj = {};

    retObj['getState'] = function() {
        var url = "/botvac/";
        return $http.get(url).then(function(response) {
            console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    return retObj;

}]);

app.controller('botvacCtrl', ['$http', 'botvacHttpFactory', '$interval', function($http, botvacHttpFactory, $interval) {
    var bv = this;
    bv.title = 'Botvac';
    bv.fetchingData = false;
    bv.botvacData = {};

    window.testScope = function() {
        console.log(bv);
    };

    
    bv.init = function() {
        bv.fetchingData = true;
        // Make HTTP Request to Fetch Lights Data
        var _prom1 = botvacHttpFactory.getState();

        _prom1.then(function(response) {
            console.log(response.data);
            bv.botvacData = response.data; 
            bv.fetchingData = false;
        });
    }

    bv.init();
}]);

app.component('botvacComp', {
    controller: 'botvacCtrl',
    controllerAs: 'bv',
    templateUrl: 'webapp/botvac/botvac.tpl.html'
});


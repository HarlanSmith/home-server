/*******************
| ROUTER COMPONENT |
*******************/

app.factory('routerHttpFactory', ['$http', function($http) {
    var retObj = {};

    retObj['getRouter'] = function() {
        var url = "/router/";
        return $http.get(url).then(function(response) {
            console.log(response);
            return response;
        }, function(error) {
            console.log(error);
        });
    };

    return retObj;

}]);

app.controller('routerCtrl', ['$http', 'routerHttpFactory', '$interval', function($http, routerHttpFactory, $interval) {
    var rt = this;
    rt.title = 'Router';
    rt.fetchingData = false;
    rt.routerData = [];
    rt.connFilter = {};
    rt.connFilter['active'] = 'all';
    rt.connFilter['check'] = function(conn) {
        if (rt.connFilter.active === 'all') {
            return true;
        } else {
            return conn.type === rt.connFilter.active;
        }
    };
    rt.connFilter['set'] = function(type) {
        rt.connFilter['active'] = type;
    };

    window.testScope = function() {
        console.log(rt);
    };

    rt.mapResponse = function(response) {
        var retArr = [];
        angular.forEach(response, function(value, index) {
            var retInnerDict = {};
            var nameMap = ['signal','ip','name','mac','type','link_rate'];
            angular.forEach(value, function(innerValue, innerIndex) {
                retInnerDict[nameMap[innerIndex]] = innerValue;
            });
            retArr.push(retInnerDict);
        });
        console.log(retArr);
        return retArr;
    };
    
    rt.init = function() {
        rt.fetchingData = true;
        // Make HTTP Request to Fetch Lights Data
        var _prom1 = routerHttpFactory.getRouter();

        _prom1.then(function(response) {
            console.log(response.data);
            // Map Inner Response Arrays to Dictionary Object
            rt.routerData = rt.mapResponse(response.data); 
            rt.fetchingData = false;
        });
    }

    rt.init();
}]);

app.component('routerComp', {
    controller: 'routerCtrl',
    controllerAs: 'rt',
    templateUrl: 'webapp/router/router.tpl.html'
});


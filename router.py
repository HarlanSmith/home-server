from flask import Blueprint, jsonify
from pynetgear import Netgear
import os
Router = Blueprint('Router', __name__)

NETGEAR = {
    'Account': {
        'Username': os.environ['NetgearUser'],
        'Password': os.environ['NetgearPassword'],
        'Host': os.environ['NetgearHost'],
        'Port': os.environ['NetgearPort'],
    },
    'r': None
}


def check_router():
    if NETGEAR['r'] is None:
        NETGEAR['r'] = Netgear(NETGEAR['Account']['Password'], NETGEAR['Account']['Host'], NETGEAR['Account']['Username'])
    return NETGEAR['r']


def get_router_data(router):
    resObj = []
    for i in router.get_attached_devices():
        resObj.append(i)
    return resObj


@Router.route('/')
def Router_Data():
    router = check_router()
    routerData = get_router_data(router)

    return jsonify(routerData)


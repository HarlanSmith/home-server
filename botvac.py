from flask import Blueprint, jsonify
from pybotvac import Account, Robot
import os

Botvac = Blueprint('Botvac', __name__)

NEATO = {
	'Account': {
		'Username': os.environ['NeatoUser'],
		'Password': os.environ['NeatoPassword']
	},
	'Robot': {
		'Name': None,
		'Serial': None,
		'Secret': None
	},
}


def check_robot():
    if NEATO['Robot']['Name'] is None:
        for robot in Account(NEATO['Account']['Username'], NEATO['Account']['Password']).robots:
            NEATO['Robot']['Name'] = robot.name
            NEATO['Robot']['Serial'] = robot.serial
            NEATO['Robot']['Secret'] = robot.secret
            return robot
    else:
        return Robot(NEATO['Robot']['Serial'], NEATO['Robot']['Secret'], NEATO['Robot']['Name'])


@Botvac.route('/')
def Vacuums():
    bot = check_robot()
    botData = bot.state

    return jsonify(botData)


@Botvac.route('/<string:command>')
def Vacuum_Command(command):
    bot = check_robot()   
    print(command)

    botData = {command: True}
 
    if command == 'start':
        botCommand = bot.start_cleaning()
    elif command == 'stop':
        botCommand = bot.stop_cleaning()
    elif command == 'pause':
        botCommand = bot.pause_cleaning()
    elif command == 'home':
        botCommand = bot.send_to_base()
    else:
        botCommand = False
        botData = bot.state

    print(botCommand)
    return jsonify(botData)

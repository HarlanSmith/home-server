## Home Automation Dashboard

Personal home dashboard built off of Flask and Angularjs. [Screenshot](https://i.imgur.com/TXD7jzh.png)

## Motivation

I've been getting more interested in home automation and IoT products and decided to build out a local web application to control all of my home integrations. There are some services (like Home Assistant) that do most of this automatically, but that is no fun.


## Current Integrations

* Philips Hue Lights
* Wemo Switch
* Netgear Router
* Neato Connected Botvac
* Dark Sky Weather API

## Future Integrations
* Amazon Alexa Voice Control
* Ecobee3 Smart Thermostat
* Custom Trigger for Automatic Litterbox
* Local News
* SmartThings Hub

## Setup
* Note: requires python3
* Pip install requirements.txt (preferably in a virtualenv)
* Manually install botvac library
* Setup evironment variables (see next section)
* Run through gunicorn (or flask dev server for testing)

## Environment Variables
* HueBridge=???
* HueUser=???
* WemoIp=???
* WemoPort=???
* NetgearUser=???
* NetgearPassword=???
* NetgearHost=???
* NetgearPort=???
* NeatoUser=???
* NeatoPassword=???
* WeatherKey=???

